require 'fileutils'

def defaults(style)
  style = style.to_s.downcase.to_sym
  styles = {}
  styles[:default] = [
    # :whole to apply to filename + ext
    # :global uses gsub instead of sub
    # syntax: [optn, optn, …, from => to]
    # where optn are symbols as laid out above, from is a 'needle' input to (g)sub, and to is a 'replace_with' input to (g)sub
    # i.e. from can be a regex or a string, to can be a string (optionally including \# references to regex matches)
    [:global, '.' => ' '],
    [:global, /(\b|\[)ettv(\]|\b)/i => ''],
    [:global, /(\b|\[)gwc(\]|\b)/i => ''],
    [:global, /\bhdtv\b/i => ''],
    [:global, /(-|\b)lol\b/i => ''],
    [:global, /(-|\b)AFG\b/i => ''],
    [:global, /(-|\b)FUM\b/i => ''],
    [:global, /(-|\b)FLEET\b/ => ''],
    [:global, /(-|\b)TLA\b/i => ''],
    [:global, /(-|\b)KILLERS\b/ => ''],
    [:global, /(-|\b)DEFiNE\b/ => ''],
    [:global, /(-|\b)DIMENSION\b/ => ''],
    [:global, /(-|\b)DEFLATE\b/ => ''],
    [:global, /(-|\b)BATV\b/ => ''],
    [:global, /(-|\b)PSA\b/ => ''],
    [:global, /(-|\b)2HD\b/i => ''],
    [:global, /(-|\b)MeGusta\b/i => ''],
    [:global, /(-|\b)mint\b/i => ''],
    [:global, /(-|\b)BAJSKORV\b/i => ''],
    [:global, /\bINTERNAL\b/ => ''],
    [:global, /\bE-Subs\b/ => ''],
    [:global, /\bAAC\b/ => ''],
    [:global, /\bHEVC\b/ => ''],
    [:global, /\bh264\b/i => ''],
    [:global, /\brepack\b/i => ''],
    [:global, /\bWEB(-DL)?\b/ => ''],
    [:global, /\bPROPER\b/ => ''],
    [:global, /\bWEBRip\b/ => ''],
    [:global, /\bXviD\b/i => ''],
    [:global, /\b2ch\b/i => ''],
    [:global, /\bx264\b/i => ''],
    [:global, /\bx265\b/i => ''],
    [/\b720p\b/i => ''],
    [/\b1080p\b/i => ''],
    #[:global, /\b-\b/ => ''],
    [/\b(\d{4}) (\d{3,})\b/ => '\2'],
    [/\b(\d{1,2})(\d{2})\b/ => '\1x\2'],
    [/\bS(\d{2})E(\d{2})\b/i => '\1x\2'],
    [/\b0(\d)x(\d{2})\b/i => '\1x\2'],
    #[/\b(\d{2})x0(\d)\b/ => '\1x\2'],
    [:global, /\s{2,}/ => ' '],
    [/\b(US)(\s+\d{1,2}x\d{1,2})/i => 'US\2'],
    [/\b(UK)(\s+\d{1,2}x\d{1,2})/i => 'UK\2'],
    [/(\s+)$/ => ''],
  ]
  styles[:film] = [
    [:global, '.' => ' '],
    [:global, /(-|\b)ShAaNiG\b/i => ''],
    [:global, /(-|\b)ETRG\b/i => ''],
    [:global, /(-|\b)JYK\b/i => ''],
    [:global, /(-|\b)NEZU\b/i => ''],
    [:global, /\bVPPV\b/i => ''],
    [:global, /\biExTV\b/i => ''],
    [:global, /\bBLURAY\b/i => ''],
    [:global, /\bDTS\b/i => ''],
    [:global, /\bBRRip\b/i => ''],
    [:global, /\bWEB(-DL)?\b/ => ''],
    [:global, /\bWEBRip\b/ => ''],
    [:global, /\bHDRIP\b/i => ''],
    [:global, /\bAAC\b/ => ''],
    [:global, /\bx264\b/i => ''],

    [/(?<!\()((19|20)\d{2})(?!=(\)))/ => '(\1)'],
    [/(?<!\()720p(?!=(\)))/ => '(720p)'],
    [/(?<!\()1080p(?!=(\)))/ => '(1080p)'],

    [:global, /\)\(/ => ') ('],
    [:global, /\s{2,}/ => ' '],
    [/(\s+)$/ => ''],
  ]
  return styles[style] if styles.key?(style)
  puts "Style '#{style}' not found in: #{styles.keys * ', '}"
  puts "Returning default style."

  styles[:default]
end

# fetches a "--str=val" argument from an argument array (returns default_val if "--str" without "=", returns nil if not "--str")
def get_val_from_args(args, str, default_val=nil)
  args = args.dup
  val_str = args.detect{|i|i.start_with?('--' + str)}
  if val_str
    if val_str['=']
      chosen_val = val_str.split('=')[1..-1] * '='
      args.delete(val_str)
    else
      arg_index = args.index(val_str)
      next_arg = args[arg_index+1]
      unless next_arg && next_arg.start_with?('-')
        chosen_val = next_arg
        args.delete_at(arg_index+1)
      end
      chosen_val ||= default_val
      args.delete_at(arg_index)
    end
  else
    chosen_val = nil
  end
  return args, chosen_val
end
# fetches a boolean from an argument array (returns !default if "--str" found, else default)
def get_bool_from_args(args, str, default=false)
  args = args.dup
  if args.index('--' + str)
    args.delete('--' + str)
    return args, !default
  else
    return args, default
  end
end

def main(args)
  args = (args.is_a?(String) ? [args] : args)

  default_style = 'default'
  untitleized_words = ['the', 'of', 'a', 'to', 'US', 'UK']

  args, help_mode = get_bool_from_args(args, 'help')
  if help_mode
    puts "Usage: filenamer.rb [options] [transformations]"
    puts
    puts "Options:"
    puts "--dry – puts the program into dry-run mode, so it doesn't perform any changes but outputs what it would do"
    puts "--style=[style] – sets the 'style mode' to use (--style defaults to 'default')"
    puts "--no-style – disables the use of a style (overrides --style=, by default uses 'default' style)"
    puts "--regex – puts the transformations into regex mode (not currently implemented, maybe should be done by /\" instead?)"
    puts "--no-titleize – makes the outputted filenames not 'titleized'"
    puts "--all – makes the renamer apply to directories as well as files (defaults to just files)"
    puts "--exts=ext1,ext2,… – only rename entries with the specified file extensions"
    puts
    puts "Transformations not yet implemented; use 'style's instead."
    return
  end

  args, dry_run = get_bool_from_args(args, 'dry')
  fileutils = dry_run ? FileUtils::DryRun : FileUtils

  args, chosen_style = get_val_from_args(args, 'style', default_style)
  args, allow_style = get_bool_from_args(args, 'no-style', true)
  if allow_style
    chosen_style ||= default_style # if style, default to default (default)
  else
    chosen_style = nil # if no style, never styles
  end

  args, regex_mode = get_bool_from_args(args, 'regex', false)
  args, titleize = get_bool_from_args(args, 'no-titleize', true)
  args, files_only = get_bool_from_args(args, 'all', true)
  file_mode = files_only ? :file : :all

  args, exts = get_val_from_args(args, 'exts', nil)
  exts = exts.split(',').map{|i|(i.start_with?('.') ? i[1..-1] : i)} if exts

  unless chosen_style
    abort("Running filenamer without a style is not supported yet.")
    #str_from = args.shift
    #str_to = args.shift
    #puts "#{str_from} => #{str_to}"
    # and if they select a style and give transformations, do the style first then transformations last! (allow this)
  end
  style_changes = defaults(chosen_style)
  unless style_changes
    abort("No style found for #{chosen_style}")
  end

  Dir.foreach('.') do |filename|
    next if filename == '.' || filename == '..'
    next if File.directory?(filename) && file_mode == :file
    next if exts && (!filename['.'] || !exts.include?(filename.split('.').last))
    puts "Doing: #{filename}"
    old_filename = filename

    style_changes.each do |change|
      if change.is_a?(Array)
        global = change.index(:global) ? true : false
        whole = change.index(:whole) ? true : false
        change_bit = change.last
      elsif change.is_a?(Hash)
        global = false
        whole = false
        change_bit = change
      end
      method_send = (global ? :gsub : :sub)
      change_bit.each do |from_match, to_match|
        file_part = (whole ? filename : File.basename(filename, '.*'))
        new_file_part = file_part.send(method_send, from_match, to_match)
        filename = filename.sub(file_part, new_file_part)
      end
    end
    if titleize && (!filename.start_with?('.') || filename[1..-1]['.'])
      # filename = filename.downcase
      name_part = File.basename(filename, '.*')
      old_namepart = name_part

      name_part = name_part[0].upcase + name_part[1..-1]
      name_part.scan(/(?:^|(?<=\s))(\w+)(?:$|(?=\s))/).each do |word|
        word = word.first if word.is_a?(Array) && word.length == 1
        next if name_part.start_with?(word)
        next if untitleized_words.include?(word)
        upper_word = word[0].upcase + word[1..-1].downcase
        name_part = name_part.sub(/\b#{word}\b/, upper_word)
      end
      filename = filename.sub(old_namepart, name_part)
    end
    next if old_filename == filename

    puts "=> #{filename}"
    temp_filename = filename + '_temp'
    fileutils.mv(old_filename, temp_filename) # move to temp filename to bypass case changing ('same path and dest' no ugh Windows)
    fileutils.mv(temp_filename, filename)
  end
end
if __FILE__ == $0
  main(ARGV)
end
